# Learning Kubernetes

# Kubespray and Vagrant

This is a small project deploy some a cluster of machines using Vagrant. I used this to 
create a Kubernetes Cluster to experiement with on my local machine. 

To use it you must do the following: 

1. Create a local.vars file. It will contain the variables for the ansible provisioning. A sample is included
   to show all the variables that you will need to run the provisioning playbook. 

   ```
   cp local.vars.sample local.vars
   vi local.vars
   ``` 
   
   A python script hashpassword.py was included to help you hash your password for your local vars. This password will be the password for the user in the machines.

   ```
   $ python hashpassword.py
   What Password: somepassword
   $6$emrg2frntkxLE0wu$SFR8pMbeLFLLJRkWBQv8dxyMTxaS3tyCW7TbOw19tV2yf03Pgs1q4QdV1nZwpNnLwAebWfbdYxOSofCrtZYq2/
   ```

2. Create a local file called vault.pwf and add your vault password to this file. This file is excluded from git and will
   make it easier for you to run the vagrant machine without having to enter in the password. 

   ``` 
   vi vault.pwf 
   ``` 

3. Encrypt your local.vars file so it cannot be read by anyone. 

   ``` 
   ansible-vault encrypt local.vars --vault-password-file=vault.pwf
   ```

4. Use vagrant to create the vm's and provision them.

   ``` 
   vagrant up 
   ```

5. test connecting to the nodes

Test node-1

    ```
    ssh -A 10.0.0.11
    ```

Test bastion host
    ```
    ssh -A 10.0.0.2
    ```

6. Test the dns agent by pinging one of the other nodes:

    ```
    ping node-1.local
    ```

